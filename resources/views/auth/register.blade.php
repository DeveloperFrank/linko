@extends('layouts.master')

@section('content')
	<div class="container text-center">
		<div class="col-xs-4"></div>
		<div class="panel panel-default col-xs-4 panel-login">
			<form method="POST" action="/auth/register">
			    {!! csrf_field() !!}

			    <div class="row">
			    	<div class="col-xs-3">
			    		<label for="">Email</label>	
			    	</div>
			        <div class="col-xs-9">
			    		<input type="email" class="form-control" name="email" value="{{ old('email') }}">
			    	</div>
			    </div>
			    <br />
			    <div class="row">
			    	<div class="col-xs-3">
			    		<label for="">Nombre</label>	
			    	</div>
			        <div class="col-xs-9">
			    		<input type="name" class="form-control" name="name" value="{{ old('name') }}">
			    	</div>
			    </div>
			    <br />
			    <div class="row">
			    	<div class="col-xs-3">
			    		<label for="">Password</label>	
			    	</div>
			        <div class="col-xs-9">
			        	<input type="password" class="form-control" name="password">	
			        </div>
			    </div>
				<br />
				<div class="row">
			    	<div class="col-xs-3">
			    		<label for="">Confirm Password</label>	
			    	</div>
			        <div class="col-xs-9">
			        	<input type="password" class="form-control" name="password_confirmation">	
			        </div>
			    </div>
				<br />
			    <div class="row">
				    <div class="col-xs-6">
				        <button class="btn btn-success" type="submit">Registrar</button>
				    </div>
			    </div>
			</form>
		</div>
		<div class="col-xs-4"></div>
	</div>
@stop