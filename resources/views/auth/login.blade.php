@extends('layouts.master')

@section('content')
	<div class="container text-center">
		@if (count($errors))
				    <ul>
				        @foreach($errors->all() as $error)
				        <div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  		<strong>{{$error}}</strong>
						</div>
				        @endforeach
				    </ul>
				@endif
		<div class="col-xs-4"></div>
		<div class="panel panel-default col-xs-12 col-md-4 panel-login">
			<form method="POST" action="/auth/login">
			    {!! csrf_field() !!}
			    <div class="row">
			    	<div class="col-xs-3">
			    		<label for="">Email</label>	
			    	</div>
			        <div class="col-xs-9">
			    		<input type="email" class="form-control" name="email" value="{{ old('email') }}">
			    	</div>
			    </div>
			    <br />
			    <div class="row">
			    	<div class="col-xs-3">
			    		<label for="">Password</label>	
			    	</div>
			        <div class="col-xs-9">
			        	<input type="password" class="form-control" name="password" id="password">	
			        </div>
			    </div>
				<br />
			    <div class="row">
			    	<div class="col-xs-6 text-center">
				        <input type="checkbox" name="remember"> Remember Me
				    </div>
				    <div class="col-xs-6">
				        <button class="btn btn-success" type="submit">Login</button>
				    </div>
			    </div>
			</form>
		</div>
		<div class="col-xs-4"></div>
	</div>
@stop