<html>
    <head>
        <title>Be right back.</title>

        <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

        <style>
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 80;
                font-family: 'Lato';
            }

            .return-error{
                text-decoration:none;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 42px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                    <strong>Opss... La página que buscas no fue encontrada!!</strong>
                    <p>
                        <strong>
                            <small>
                                <a href="/" class="return-error">Regresar</a>
                            </small>
                        </strong>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
