@extends('layouts.master')

<?php $message = Session::get('message')?>

@section('content')
	<div class="container">
		@if($message == 'store')
			<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  		<strong>Success!</strong> La Url se a guardadó exitosamente.
			</div>
		@endif
		<hr />
		<div class="row">
			@foreach($links as $link)
			<div class="col-xs-12 col-md-4">
				<div class="panel panel-primary item">
					<div class="panel-heading"><p><strong>{{$link->title}}</strong></p></div>
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-10">
								<p>{{$link->description}}</p> 
							</div>
							<div class="col-xs-1">
								<a href="{{$link->url}}" target="_blank"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<div class="row">
							<div class="col-xs-4">
								{!!link_to_route('link.edit',$title ='', $parameter = $link->id, $attribute = ['class' => 'glyphicon glyphicon-pencil'])!!}
							</div>
							<div class="col-xs-8">
								<small><strong>Creado el: {{$link->created_at}}</strong></small>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		{!! $links->render() !!}
	</div>
@stop

