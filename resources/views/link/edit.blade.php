@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-4">
				{!!Form::model($link, ['route' => ['link.update', $link->id], 'method' => 'PUT'])!!}
					{!! csrf_field() !!}
					<div class="form-group">
						{!!Form::label('Titulo:')!!}
						{!!Form::text('title', null, ['class'=>'form-control', 'placeholder'=>'Ingrese titulo de la url'])!!}
					</div>
					<div class="form-group">
						{!!Form::label('Url:')!!}
						{!!Form::text('url', null, ['class'=>'form-control', 'placeholder'=>'Ingrese la dirección url'])!!}
					</div>
					<div class="form-group">
						{!!Form::label('Categoria:')!!}
						{!!Form::select('category', $category, $link->category_id, ['class' => 'form-control'])!!}
						<!-- {!!Form::select('category', $category, null, ['class'=>'form-control'])!!} -->
					</div>
					<div class="form-group">
						{!!Form::label('Descripción:')!!}
						{!!Form::text('description', null, ['class'=>'form-control', 'placeholder'=>'Ingrese una descripción de la url'])!!}
					</div>
					{!!Form::submit('Actualizar', ['class'=>'btn btn-info'])!!}
				{!!Form::close()!!}	
			</div>
			<div class="col-xs-4"></div>
		</div>
	</div>
@stop