@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-4"></div>
			<div class="col-xs-4">
				{!!Form::open(['route'=>'user.store', 'method'=>'POST'])!!}
					<div class="form-group">
						{!!Form::label('Nombre:')!!}
						{!!Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Ingrese su nombre'])!!}
					</div>
					<div class="form-group">
						{!!Form::label('Email:')!!}
						{!!Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Ingrese su correo electronico'])!!}
					</div>
					<div class="form-group">
						{!!Form::label('Password:')!!}
						{!!Form::password('password', array('class' => 'form-control', 'placeholder' => 'Escriba un password'))!!}

					</div>
					{!!Form::submit('Registrar', ['class'=>'btn btn-success'])!!}
				{!!Form::close()!!}	
			</div>
			<div class="col-xs-4"></div>
		</div>
	</div>
@stop