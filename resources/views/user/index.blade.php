@extends('layouts.master')

<?php $message = Session::get('message')?>

@section('content')

	<div class="container">

	@if($message == 'store')
		<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	  		<strong>Success!</strong> El usuario se a creado exitosamente.
		</div>
	@endif



		<div class="col-xs-3"></div>
		<div class="col-xs-6">
			<table class="table table-striped table-bordered">
			    <thead>
			      <tr>
			        <th>Usuario</th>
			        <th>Email</th>
			        <th colspan="2">Acciones</th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>{{$user_find->name}}</td>
			        <td>{{$user_find->email}}</td>
			        <td><a href="#">Editar</a></td>
			        <td><a href="#">Eliminar</a></td>
			      </tr> 			    
			
			    </tbody>
			  </table>
		</div>
		<div class="col-xs-3"></div>	
	</div>
@stop