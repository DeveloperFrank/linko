@extends('layouts.master')

@section('content')
	<div class="container">
		{!!Form::open(['route' => 'category.store', 'method' => 'POST'])!!}
			@include('category.partials.fields')
		{!!Form::submit('Crear', ['class' => 'btn btn-info'])!!}
	</div>
@stop