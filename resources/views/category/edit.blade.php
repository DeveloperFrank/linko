@extends('layouts.master')

@section('content')
	<div class="container">
		{!!Form::model($category, ['route' => ['category.update', $category->id], 'method' => 'PUT'])!!}
			@include('category.partials.fields')
		{!!Form::submit('Actualizar', ['class' => 'btn btn-info'])!!}
	</div>
@stop