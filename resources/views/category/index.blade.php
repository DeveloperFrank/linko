@extends('layouts.master')

<?php $message = Session::get('message')?>

@section('content')

	<div class="container">
		@if($message == 'store')
			<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  		<strong>Success!</strong> La categoría se creó exitosamente.
			</div>
		@endif
		@if($message == 'update')
			<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  		<strong>La categoría se actualizó exitosamente.</strong>
			</div>
		@endif
		<div class="row">
			<div class="col-xs-3">
				<h3>Categorías actuales</h3>
			</div>
			<div class="col-xs-6">
				<a href="category/create" class="btn btn-info btn-xs btn-cat">Crear categoría</a>
			</div>
		</div>
		<hr />
		<div class="row">
			@foreach($categories as $category)
				<div class="panel panel-default col-xs-3 item_category">
					<br>
					<div class="row">
						<div class="col-xs-10">
							{{$category->name}}	
						</div>
						<div class="col-xs-2">
							{!!link_to_route('category.edit',$title ='', $parameter = $category->id, $attribute = ['class' => 'glyphicon glyphicon-pencil'])!!}		
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@stop