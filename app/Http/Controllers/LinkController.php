<?php

namespace Linko\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Linko\Http\Requests;
use Linko\Http\Controllers\Controller;

//se carga el modelo para llenar el dropdown list
use Linko\Category;
use Linko\Link;

class LinkController extends Controller
{

    public function index()
    {
        $categories = \DB::table('category')
                            ->select(['id','name'])
                            ->get();

        $links = \DB::table('links')
                        ->select(['id', 'title', 'url', 'description', 'created_at'])
                        ->orderBy('created_at', 'desc')
                        ->paginate(9);


        return view('link.index', compact('links', 'categories')); 
    }


    public function create()
    {
        $user = Auth::user();
        if ($user)
        {
            //obtener datos para llenar select:dropdown list
            $category = \Linko\Category::lists('name', 'id');
            return view('link.create', compact('category'));
        } else 
        {
            return redirect('auth/login');
        }
        
    }


    public function store(Request $request)
    {
        \Linko\Link::create([
            'title' => $request['title'],
            'url' => $request['url'],
            'description' => $request['description'],
            'user_id' => Auth::user()->id,
            'category_id' => $request['category'],
        ]);

        return redirect('/link')->with('message', 'store');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $link = \Linko\Link::findOrFail($id);

        $category = \Linko\Category::lists('name', 'id');
        
        return view('link.edit', compact('link', 'category'));
    }


    public function update(Request $request, $id)
    {
        $link = Link::find($id);
        $link->fill($request->all());

        //obtenemos el id de la categoría
        $data = request('category');

        //llenamos el campo con el id de la categoría elegída en el select
        $link->fill(['category_id' => $data]);
        $link->save();

        return redirect('link')->with('message', 'update');
    }


    public function destroy($id)
    {
        //
    }
}
