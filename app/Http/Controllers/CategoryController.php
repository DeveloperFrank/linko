<?php

namespace Linko\Http\Controllers;

use Illuminate\Http\Request;

use Linko\Http\Requests;
use Linko\Http\Controllers\Controller;
use Linko\Category;
use Linko\Link;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = \Linko\Category::All();
        return view('category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Linko\Category::create([
            'name' => $request['name']
        ]);

        return redirect('category')->with('message', 'store');
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        

        //buscar categoría por id
        $categories_find = Category::findOrFail($id);

        $links = \DB::table('links')->where('category_id', '=', $id)->get();
        //dd($links);

        return view('category.show', compact('categories_find', 'links', 'categories'));
    }


    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->fill($request->all());
        $category->save();

        return redirect('category')->with('message', 'update');
    }


    public function destroy($id)
    {
        //
    }
}
