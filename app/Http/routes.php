<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




Route::resource('/', 'LinkController');
Route::resource('user', 'UserController');
Route::resource('link', 'LinkController');

//restringiendo rutas con el middleware auth
Route::group(['middleware' => 'auth'], function() {
	Route::resource('category', 'CategoryController');
	Route::resource('user', 'UserController');
	Route::resource('link', 'LinkController');
});

//auth
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
