<?php

namespace Linko;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $fillable = ['name'];

    //para deshabilitar los campos de creado y editado.
    public $timestamps = false;

    public function links()
	{
	    return $this->hasMany('Linko\Link');
	}
}
