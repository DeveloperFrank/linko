<?php

namespace Linko\Providers;

use Illuminate\Support\ServiceProvider;
use Linko\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Variable global para cargar en todas las vistas.
        $categories = Category::All();
        view()->share('categories', $categories);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
