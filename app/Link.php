<?php

namespace Linko;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'links';

    protected $fillable = ['title', 'url', 'description', 'user_id', 'category_id'];

    public function categorys()
	{
	    return $this->belongsTo('Linko\Category', 'foreign_key');
	}

	public function users()
	{
	    return $this->belongsTo('Linko\User', 'foreign_key');
	}

}
