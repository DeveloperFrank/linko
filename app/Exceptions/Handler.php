<?php

namespace Linko\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{

    protected $dontReport = [
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
    ];


    public function report(Exception $e)
    {
        return parent::report($e);
    }

    public function render($request, Exception $e)
    {
        dd($e);

        if ($e->getStatusCode() == 404) {
            
            return response()->view('errors.503', [], 404);
        }

        return parent::render($request, $e);
    }
}
